using System;
using System.Collections.Generic;
using Web.BusinessLayer;
using Web.DataLayer;
using Microsoft.AspNetCore.Mvc;
using WebAPIDesign.Models;

namespace TodoWebAPIDesign.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        ProductContext context_;
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new ProductsRepository(context_).GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return new ProductsRepository(context_).GetById(id);

        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
            return Created($"api/Values/{value}", value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
            return Accepted(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            return NoContent();
        }
    }
}
