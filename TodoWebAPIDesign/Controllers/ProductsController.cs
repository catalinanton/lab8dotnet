﻿using System;
using System.Collections.Generic;
using Web.BusinessLayer;
using Web.DataLayer;
using Microsoft.AspNetCore.Mvc;
using WebAPIDesign.Models;

namespace WebAPIDesign.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductsRepository _repository;

        public ProductsController(IProductsRepository repository)
        {
            _repository=repository;
        }

        [HttpGet]
        public ActionResult<IReadOnlyList<Products>> Get()
        {
            return Ok(this._repository.GetAll());
        }

        [HttpGet("{id}", Name = "GetById")]
        public ActionResult<Products> Get(Guid id)
        {
            return Ok(this._repository.GetById(id));
        }
        [HttpPost]
        public ActionResult<Products> Post([FromBody] CreateProductsModel createProductsModel)
        {
            if (createProductsModel == null)
            {
                return BadRequest();
            }

            Products products = new Products(createProductsModel.Name, createProductsModel.Price, createProductsModel.Pieces, createProductsModel.Description);
            this._repository.Create(products);

            return CreatedAtRoute("GetById", new { id = products.Id }, products);
        }

    }
}