﻿using System.ComponentModel.DataAnnotations;

namespace WebAPIDesign.Models
{
    public class CreateProductsModel
    {
        [Required]
        [StringLength(50)]
        public string Name { get; private set; }

        [Required]
        public double Price { get; private set; }

        [Required]
        public int Pieces { get; private set; }

        [Required]
        public double Total { get; private set; }

        [Required]
        [StringLength(100)]
        public string Description { get; private set; }
    }
}
