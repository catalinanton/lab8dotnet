using Microsoft.EntityFrameworkCore;

namespace Web.DataLayer
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Products> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
      //      modelBuilder.Entity<Products>().Property(t => t.IsCompleted).IsRequired();
            modelBuilder.Entity<Products>().Property(t => t.Description).IsRequired().HasMaxLength(50);
        }
    }
}