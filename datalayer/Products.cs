﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.DataLayer
{
    //– Id, Name, Price, Pieces, Total
    public class Products
    {

        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public double Price { get; private set; }

        public int Pieces { get; private set; }

        [NotMapped()]
        public double Total { get; private set; }

        public string Description { get; private set; }

 

        public Products(string name, double price, int pieces, string description)
        {
            Id = Guid.NewGuid();
            Name = name;
            Price = price;
            Pieces = pieces;
            Description = description;
        }

        public void AttachPiece()
        {
            this.Pieces++;
        }

        public void DetachPiece(Guid id)
        {
            if (this.Pieces > 0)
            {
                this.Pieces--;
            }
        }
    }
}
