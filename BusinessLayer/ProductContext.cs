using Microsoft.EntityFrameworkCore;

namespace Web.DataLayer
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().Property(t => t.IsCompleted).IsRequired();
            modelBuilder.Entity<Product>().Property(t => t.Description).IsRequired().HasMaxLength(50);
        }
    }
}