using Microsoft.EntityFrameworkCore;

namespace Web.DataLayer
{
    public class ShoppingCartContext : DbContext
    {
        public ShoppingCartContext(DbContextOptions<ShoppingCartContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<ShoppingCart> ShoppingCarts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ShoppingCart>().Property(t => t.IsCompleted).IsRequired();
            modelBuilder.Entity<ShoppingCart>().Property(t => t.Description).IsRequired().HasMaxLength(50);
        }
    }
}