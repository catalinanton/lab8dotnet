﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

//api/cities/1/pois/1
namespace Web.DataLayer
{
    //Id, Date, Description,Total
    public class ShoppingCart
    {
        public Guid Id { get; private set; }

        public DateTime Date{ get;private set; }

        [NotMapped()]
        public double Total;

        public string Description{ get;private set; }

        private ShoppingCart()
        {
            //EF
        }

        public ShoppingCart(DateTime date,  string description)
        {
            Date = date;
            Description = description;
            Total = 0;
        }
    }
}
