﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.DataLayer;

namespace Web.BusinessLayer
{
    public interface IShoppingCartRepository
    {
        void Create(Products products);
        IReadOnlyList<Products> GetAll();
        Products GetById(Guid id);
    }
}
