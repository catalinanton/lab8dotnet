﻿using System;
using System.Collections.Generic;
using Web.DataLayer;

namespace Web.BusinessLayer
{
    public interface IProductsRepository
    {
        void Create(Products products);
        IReadOnlyList<Products> GetAll();
        Products GetById(Guid id);
    }
}