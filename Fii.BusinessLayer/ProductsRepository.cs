﻿using Web.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using Web.BusinessLayer;

namespace Web.BusinessLayer
{
    public class ProductsRepository : IProductsRepository
    {
        private readonly ProductContext context;

        public ProductsRepository(ProductContext context)
        {
            this.context = context;
        }
        public void Create(Products products)
        {
            if (products!=null)
            {
                this.context.Products.Add(products);
                this.context.SaveChanges();
            }
        }

        public Products GetById(Guid id)
        {
            return this.context.Products.FirstOrDefault(t => t.Id == id);
        }

        public IReadOnlyList<Products> GetAll()
        {
            return this.context.Products.ToList();
        }
    }
}
